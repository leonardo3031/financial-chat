export interface ChatUser {
  connectionId: string;
  username: string;
}
