﻿using FinancialChat.Core.Contracts;
using FinancialChat.Core.Helpers.Messaging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinancialChat.Messaging.Hubs
{
    [Authorize]
    public class ChatHub : Hub
    {
        private readonly IMessageSender _messageSender;

        // Chat users and messages could be saved to a database, I'm saving them this way for simplicity
        private static IList<ChatUser> _chatUsers = new List<ChatUser>();
        private static IList<ChatMessage> _messages = new List<ChatMessage>();

        public ChatHub(IMessageSender messageSender)
        {
            _messageSender = messageSender;
        }

        private void AddNewMessage(ChatMessage message)
        {
            _messages.Add(message);

            if (_messages.Count > 50)
                _messages.RemoveAt(0);
        }

        public override Task OnConnectedAsync()
        {
            string userName = Context.User.Identity?.Name;
            string connectionId = Context.ConnectionId;

            if (_chatUsers.All(connectedUser => connectedUser.Username != userName))
            {
                _chatUsers.Add(new ChatUser
                {
                    ConnectionId = connectionId,
                    Username = userName
                });

                Clients.All.SendAsync("ChatUsersChanged", _chatUsers);
            }
            else
            {
                Clients.Caller.SendAsync("ChatUsersChanged", _chatUsers);
            }

            Clients.Caller.SendAsync("CurrentMessages", _messages);

            return base.OnConnectedAsync();
        }

        public async Task SendMessage(ChatMessage message)
        {
            await Clients.All.SendAsync("NewMessage", message);

            if (message.Message.Contains("/stock="))
            {
                _messageSender.SendMessage(message);
                return;
            }

            AddNewMessage(message);
        }

        public void SendBotMessage(ChatMessage message)
        {
            AddNewMessage(message);
        }

        public async Task Disconnect(string userName)
        {
            if (_chatUsers.Any(currentUser => currentUser.Username == userName))
            {
                _chatUsers = _chatUsers.Where(currentUser => currentUser.Username != userName).ToList();
                await Clients.All.SendAsync("ChatUsersChanged", _chatUsers);
            }
        }
    }
}
